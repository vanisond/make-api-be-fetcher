# syntax=docker/dockerfile:1
FROM ghcr.io/puppeteer/puppeteer:latest

# Use a custom directory for global npm packages
ENV NPM_CONFIG_PREFIX=/home/pptruser/.npm-global

# Ensure the directory exists and set the correct permissions
RUN mkdir -p /home/pptruser/.npm-global && chown -R pptruser:pptruser /home/pptruser/.npm-global

# Update PATH to include the new global packages location
ENV PATH=$PATH:/home/pptruser/.npm-global/bin

# Set working directory
WORKDIR /home/pptruser

# Copy package.json and package-lock.json before other files
# Utilize Docker cache to save re-installing dependencies if unchanged
COPY package*.json ./

# Install dependencies
RUN npm ci

# Copy all files
COPY --chown=pptruser:pptruser . .

# Install TypeScript globally
RUN npm install -g typescript

# Compile TypeScript to JavaScript
RUN tsc

# Command to run the application
CMD [ "node", "dist/index.js" ]

# Expose the port the app runs on
EXPOSE ${PORT}
