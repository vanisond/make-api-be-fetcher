# URL Fetcher

A service providing a proxy for fetching web pages based on URL. It provides a simple API server with following endpoints:

- `GET /raw?url=...` - returns the page content loaded with a simple HTTP fetch (uses Axios)
- `GET /html?url=...` - returns HTML content rendered inside a headless browser (uses Puppeteer)

## Environment variables

Before running the service, you need to create an environment file `.env` in the root directory of the project. You can use the `.env.example` file as a template.

## Running the service

To run the service, you need to have [Node.js](https://nodejs.org/en/) and [npm](https://www.npmjs.com/) installed. You also need to have [Docker](https://docs.docker.com/get-docker/) in order to run the Redis database. (Although you can use any other Redis database, you just need to change the connection URL in the environment file.)

To start a Redis database, run the following command:

```bash
docker run --name make-api-db-redis -p 6379:6379 -d redis
```

Then you need to install the dependencies and start the service:

```bash
npm install
npm run start
```

Now the service should be running on [http://localhost:4001/](http://localhost:4001/).
