import express, {Express, Request, RequestHandler, Response} from "express";
import puppeteer from "puppeteer";
import Keyv from "keyv";
import axios, {AxiosError} from "axios";
import {setupCache} from "axios-cache-interceptor";
import CacheControlParser from "cache-control-parser";

import * as dotenv from "dotenv";

dotenv.config();

const app: Express = express();
const port = process.env.PORT;
const defaultHeadlessContentTTL = parseInt(
  process.env.URL_CACHE_HEADLESS_CONTENT_DEFAULT_TTL_S ?? "60"
);
const defaultRawContentTTL = parseInt(
  process.env.URL_CACHE_RAW_CONTENT_DEFAULT_TTL_S ?? "60"
);
const headlessMaxContentLength = parseInt(
  process.env.HEADLESS_MAX_CONTENT_LENGTH ?? "0"
);
const fetchMaxContentLength = parseInt(
  process.env.FETCH_MAX_CONTENT_LENGTH ?? "0"
);
const fetchMaxBodyLength = parseInt(process.env.FETCH_MAX_BODY_LENGTH ?? "0");

// @ts-ignore - @see https://github.com/arthurfiorette/axios-cache-interceptor/issues/746
const cachedAxios = setupCache(axios); // Add cache adapter to axios instance

export type CacheContentItem = {
  content: string;
  contentType?: string;
  timestamp: number;
  maxAge: number;
};

const htmlContentCache = new Keyv<CacheContentItem>(
  process.env.URL_CACHE_CONNECTION_URL,
  {
    namespace: "html-content-cache",
  }
);

const rawContentCache = new Keyv<CacheContentItem>(
  process.env.URL_CACHE_CONNECTION_URL,
  {
    namespace: "raw-content-cache",
  }
);

app.get("/", (req: Request, res: Response) => {
  res.send("Page Fetcher");
});

app.get("/html", (async (req, res) => {
  const url = req.query.url;
  if (!url || typeof url !== "string") {
    return res.status(400).send("Please provide URL as GET parameter.");
  }

  try {
    const cachedContent = await htmlContentCache.get(url);

    // Send cached content if available
    if (cachedContent) {
      console.log(`[${new Date().toISOString()}] HTML : GET (${url}) - cache hit`);
      return res
          .setHeader(
              "Cache-Control",
              CacheControlParser.stringify({
                "max-age": Math.max(
                    0,
                    Math.floor(
                        cachedContent.maxAge -
                        (Date.now() - cachedContent.timestamp) / 1000
                    )
                ),
              })
          )
          .send(cachedContent.content);
    }
  } catch (error) {
    console.error(`[${new Date().toISOString()}] HTML : GET (${url}) - cache error`, error);
  }

  // Otherwise load page using the headless browser
  const browser = await puppeteer.launch({ headless: "new" });
  const page = await browser.newPage();
  if (process.env.USER_AGENT_STRING) {
    await page.setUserAgent(process.env.USER_AGENT_STRING);
  }
  await page.goto(url, { waitUntil: "domcontentloaded" });

  const content = await page.content();
  await browser.close();

  if (content.length > headlessMaxContentLength) {
    return res.status(413).send({
      error: "Content too large",
      message: `Content length ${content.length} exceeds maximum allowed length of ${headlessMaxContentLength}`,
    });
  }

  console.log(`[${new Date().toISOString()}] HTML : GET (${url})`);
  res
    .setHeader(
      "Cache-Control",
      CacheControlParser.stringify({ "max-age": defaultHeadlessContentTTL })
    )
    .send(content);

  // Results from headless browser have a fixed TTL
  void htmlContentCache.set(
    url,
    {
      content,
      maxAge: defaultHeadlessContentTTL, // should be in seconds
      timestamp: Date.now(),
    },
      1000 * defaultHeadlessContentTTL
  );
}) as RequestHandler);

app.get("/raw", (async (req, res) => {
  const url = req.query.url;
  if (!url || typeof url !== "string") {
    return res.status(400).send("Please provide URL as GET parameter.");
  }

  try {
    const cachedContent = await rawContentCache.get(url);

    // Send cached content if available
    if (cachedContent) {
      console.log(`[${new Date().toISOString()}] RAW : GET (${url}) - cache hit`);
      if (cachedContent.contentType) {
        res.setHeader("Content-Type", cachedContent.contentType);
      }
      return res
        .setHeader(
          "Cache-Control",
          CacheControlParser.stringify({
            "max-age": Math.max(
              0,
              Math.floor(
                cachedContent.maxAge -
                  (Date.now() - cachedContent.timestamp) / 1000
              )
            ),
          })
        )
        .send(cachedContent.content);
    }
  } catch (error) {
    console.error(`[${new Date().toISOString()}] RAW : GET (${url}) - cache error`, error);
  }

  // Otherwise fetch content from origin
  try {
    const rawContentResponse = await cachedAxios.get(url, {
      maxContentLength: fetchMaxContentLength,
      maxBodyLength: fetchMaxBodyLength,
      headers: process.env.USER_AGENT_STRING
        ? {
            "User-Agent": process.env.USER_AGENT_STRING,
          }
        : undefined,
    });
    const content = rawContentResponse.data;
    const maxAge = CacheControlParser.parse(
      rawContentResponse.headers["cache-control"] ?? ""
    )?.["max-age"];
    if (rawContentResponse.headers["content-type"]) {
      res.setHeader("Content-Type", rawContentResponse.headers["content-type"]);
    }

    console.log(`[${new Date().toISOString()}] RAW : GET (${url})`);
    res
      .setHeader(
        "Cache-Control",
        CacheControlParser.stringify({
          "max-age": maxAge ?? defaultRawContentTTL,
        })
      )
      .send(content);

    void rawContentCache.set(
      url,
      {
        content,
        contentType: rawContentResponse.headers["content-type"],
        maxAge: maxAge ?? defaultRawContentTTL, // should be in seconds
        timestamp: Date.now(),
      },
        1000 * (maxAge ?? defaultRawContentTTL)
    );
  } catch (error) {
    // return 413 if maxContentLength or maxBodyLength is exceeded
    if (error instanceof AxiosError && error.code === "ERR_FR_MAX_BODY_LENGTH_EXCEEDED") {
      return res.status(413).send({
        error: "Content too large",
        message: `Content length exceeds maximum allowed length of ${fetchMaxContentLength}`,
      });
    }
    // Otherwise return 500
    res.status(500).send({
      message: "Error fetching content",
      error,
    });
  }
}) as RequestHandler);

app.get("/render", (async (req, res) => {
  const url = req.query.url;
  if (!url || typeof url !== "string") {
    return res.status(400).send("Please provide URL as GET parameter.");
  }
  const browser = await puppeteer.launch({ headless: "new" });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  // Take the screenshot
  const screenshot = await page.screenshot();

  // Close the browser
  await browser.close();

  // Set the appropriate response headers and send the screenshot
  res.setHeader("Content-Type", "image/png");
  res.setHeader(
    "Cache-Control",
    CacheControlParser.stringify({ "max-age": 24 * 60 })
  );
  res.send(screenshot);
}) as RequestHandler);

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});
